﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SelfAttestation.Web.Models;
using Microsoft.AspNetCore.Http;
using SelfAttestation.Web.Helper;
using SelfAttestation.Web.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace SelfAttestation.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly string _user;
        private readonly IAttestationService _attestationService;
        private readonly IFaqService _faqService;

        private readonly IConfiguration _config;

        public HomeController(ILogger<HomeController> logger, IHttpContextAccessor httpContextAccessor, IAttestationService attestationService, IFaqService faqService, IConfiguration config)
        {
            _logger = logger;
            _user = httpContextAccessor.HttpContext.User.Identity.Name;
            _attestationService = attestationService;
            _faqService = faqService;
            _config = config;
        }

        public IActionResult Index()
        {

            var inNetwork = Convert.ToBoolean(_config["AppSettings:InNetwork"]);

            //// if inNetwork = false, load secure auth; if inNetwork = true, simply grab the AD username.

            var userName = "";
            if (!inNetwork)
            {
                if (User.Identity.IsAuthenticated)
                {
                    userName = Authentication.cleanUsername(_user);
                }
            }
            else
            {
                userName = Authentication.cleanUsername(_user);
            }
            ViewBag.Username = userName;
            return View();
        }

        public IActionResult Legal()
        {
            return View();
        }

        [HttpGet]
        public async Task<IActionResult> Faq()
        {
            var faqInfo = new List<Faqs>();
            try
            {
                faqInfo = await _faqService.GetFaqInfo();

                if (faqInfo == null)
                {
                    var serviceUri = $"{_config["AppSettings:RestUrl"]}/{_config["AppSettings:GetFaqInfo"]}";

                    var res = GetDataFromApi(serviceUri);
                    if (res != null)
                    {
                        using (var reader = new StreamReader(res.GetResponseStream()))
                        {
                            var objText = reader.ReadToEnd();

                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            faqInfo = (List<Faqs>)JsonConvert.DeserializeObject(objText, typeof(List<Faqs>), settings);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                var faq = new Faqs
                {
                    Question = "Issue loading FAQ's at this time. Please try again later.",
                    Response = "",
                    DisplayOrder = 1,
                    CreateDt = DateTime.Now
                };

                faqInfo.Add(faq);
            }

            ViewBag.FaqInfo = faqInfo;

            return View(faqInfo);
        }
        public async Task<IActionResult> BeginSelfAttestation(string failedAttempt = null)
        {
            //var userName = Authentication.cleanUsername(_user);
            //ViewBag.Username = userName;

            var instructions = new List<Instruction>();
            try
            {
                instructions = await _attestationService.GetAttestationText();

                if (instructions == null)
                {
                    var serviceUri = $"{_config["AppSettings:RestUrl"]}/{_config["AppSettings:GetAttestationText"]}";

                    var res = GetDataFromApi(serviceUri);
                    if (res != null)
                    {
                        using (var reader = new StreamReader(res.GetResponseStream()))
                        {
                            var objText = reader.ReadToEnd();

                            var settings = new JsonSerializerSettings
                            {
                                NullValueHandling = NullValueHandling.Ignore,
                                MissingMemberHandling = MissingMemberHandling.Ignore
                            };
                            instructions = (List<Instruction>)JsonConvert.DeserializeObject(objText, typeof(List<Instruction>), settings);
                        }
                    }
                }
            }
            catch
            {
                var instruction = new Instruction
                {
                    Info = "There is an issue loading the Attestation Criteria. Please try again later."
                };

                instructions.Add(instruction);
            }

            ViewBag.Instructions = instructions;
            ViewBag.FailedAttempt = failedAttempt;

            return View();
        }


        [HttpGet]
        public ActionResult UserNotUsingLSID()
        {
            var userName = Authentication.cleanUsername(_user);
            return Json(userName);
        }
        public bool ValidateCredentials(string domain, string username, string password)
        {
            using (var context = new PrincipalContext(ContextType.Domain, domain))
            {
                return context.ValidateCredentials(username, password);
            }
        }

        [HttpPost]
        public async Task<IActionResult> SubmitAttestation(IFormCollection collection)
        {
            var userName = collection["attestUsername"];
            var pw = collection["loginPassword"].ToString();
            var isValid = false;
            if (pw != "" || pw != string.Empty)
            {
                 isValid = ValidateCredentials("bh.local", userName, pw);
            }
            else
            {
                isValid = true;
            }

            if (isValid)
            {
                var attestation = collection["attestationOption"];

                var userInfo = new UserInfo
                {
                    Username = userName,
                    Date = DateTime.Now,
                    ResultType = attestation,
                    ResultFlag = Convert.ToInt32(_config["AppSettings:ResultFlag"])
                };

                var results = await _attestationService.SubmitAttestationForm(userInfo);

                return RedirectToAction("Results", "Home", new { ResultType = userInfo.ResultType });
            }
            else
            {
                var failedAttempt = "Invalid username and / or password, please re-enter credentials.";

                return RedirectToAction("BeginSelfAttestation", "Home", new { failedAttempt = failedAttempt });
            }
        }

        public IActionResult Results(string ResultType)
        {
            ViewBag.ResultType = ResultType;
            ViewBag.Date = DateTime.Now.ToShortDateString() + " - " + DateTime.Now.ToShortTimeString();

            return View();
        }

        private static HttpWebResponse GetDataFromApi(string serviceUri, string strMethod = "GET")
        {
            HttpWebResponse webResponse = null;
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(serviceUri);
                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = strMethod;
                if (strMethod.ToLower().Equals("post"))
                    httpWebRequest.ContentLength = 0;
                httpWebRequest.Timeout = 200000000;

                webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (WebException)
            {

            }
            catch (Exception)
            {
                // ignored
            }

            return webResponse;
        }
    }
}
