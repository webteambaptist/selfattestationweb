﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelfAttestation.Web.Models
{
    public class UserInfo
    {
        public string Username { get; set; }
        public string ResultType { get; set; }
        public DateTime Date { get; set; }
        public int ResultFlag { get; set; }
    }
}
