﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelfAttestation.Web.Models
{
    public class Faqs
    {
        public int Id { get; set; }
        public string Question { get; set; }
        public string Response { get; set; }
        public int DisplayOrder { get; set; }
        public DateTime CreateDt { get; set; }
    }
}
