﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace SelfAttestation.Web.Services
{
    public class BaseService
    {
        private readonly HttpClient _client;

        public BaseService()
        {
            try
            {
                var messageHandler = new HttpClientHandler
                {
                    UseCookies = true
                };

                _client = new HttpClient(messageHandler) { Timeout = new TimeSpan(0, 0, 15) };
                _client.DefaultRequestHeaders.Accept.Clear();
                _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }

        protected async Task<TResult> GetResponse<TResult>(string serviceUri, Dictionary<string, string> addedHeaders = null)
        {
            if (addedHeaders != null)
            {
                foreach (var header in addedHeaders)
                {
                    if (_client.DefaultRequestHeaders.Contains(header.Key))
                        _client.DefaultRequestHeaders.Remove(header.Key);

                    _client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }

            try
            {
                var response = await _client.GetAsync(serviceUri);

                if (response.IsSuccessStatusCode)
                {
                    var jsonString = await response.Content.ReadAsStringAsync();
                    if (!string.IsNullOrEmpty(jsonString))
                    {
                        var returnType = typeof(TResult);
                        if (returnType == typeof(string))
                            return (TResult)Convert.ChangeType(jsonString, typeof(TResult));

                        return JsonConvert.DeserializeObject<TResult>(jsonString);
                    }
                }

            }
            catch (Exception ex)
            {
                var tags = new Dictionary<string, string> { { "serviceUri", serviceUri } };                
            }

            return default(TResult);
        }


        protected async Task<TResult> PostResponse<TResult, T>(string serviceUri, T data,
           bool requiresAuthentication = false, Dictionary<string, string> addedHeaders = null)
        {

            if (addedHeaders != null)
            {
                foreach (var header in addedHeaders)
                {
                    if (_client.DefaultRequestHeaders.Contains(header.Key))
                        _client.DefaultRequestHeaders.Remove(header.Key);

                    _client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }

            try
            {
                var json = JsonConvert.SerializeObject(data);
                try
                {
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    var response = await _client.PostAsync(serviceUri, content);

                    if (response.IsSuccessStatusCode)
                    {
                        var jsonString = await response.Content.ReadAsStringAsync();
                        if (!string.IsNullOrEmpty(jsonString))
                        {
                            return JsonConvert.DeserializeObject<TResult>(jsonString);
                        }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    throw;
                }

            }
            catch (Exception ex)
            {
                var tags = new Dictionary<string, string> { { "serviceUri", serviceUri } };
                //Crashes.TrackError(ex, tags);
            }

            return default(TResult);
        }

        protected async Task<TResult> PostResponse<TResult>(string serviceUri, FormUrlEncodedContent data,
            bool requiresAuthentication = false, Dictionary<string, string> addedHeaders = null)
        {
            //if (requiresAuthentication)
            //    SetApiAuthorizationHeaders();

            if (addedHeaders != null)
            {
                foreach (var header in addedHeaders)
                {
                    if (_client.DefaultRequestHeaders.Contains(header.Key))
                        _client.DefaultRequestHeaders.Remove(header.Key);

                    _client.DefaultRequestHeaders.Add(header.Key, header.Value);
                }
            }

            try
            {
                var response = await _client.PostAsync(serviceUri, data);

                if (response.IsSuccessStatusCode)
                {
                    var jsonString = await response.Content.ReadAsStringAsync();
                    if (!string.IsNullOrEmpty(jsonString))
                    {
                        return JsonConvert.DeserializeObject<TResult>(jsonString);
                    }
                }

            }
            catch (Exception ex)
            {
                var tags = new Dictionary<string, string> { { "serviceUri", serviceUri } };
                //Crashes.TrackError(ex, tags);
            }

            return default(TResult);
        }


    }
}
