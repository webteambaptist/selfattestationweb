﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using SelfAttestation.Web.Models;
using SelfAttestation.Web.Services.Interfaces;

namespace SelfAttestation.Web.Services
{
    public class FaqService : BaseService, IFaqService
    {
        private readonly IConfiguration _config;

        public FaqService(IConfiguration config)
        {
            _config = config;
        }

        public async Task<List<Faqs>> GetFaqInfo()
        {
            var serviceUri = $"{_config["AppSettings:RestUrl"]}/{_config["AppSettings:GetFaqInfo"]}";
            //var serviceUri = "https://ws2.bmcjax.com/SelfAttestationAPI/faq/getFaqInfo";
            return await GetResponse<List<Faqs>>(serviceUri, new Dictionary<string, string> { });
        }
    }
}
