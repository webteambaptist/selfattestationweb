﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SelfAttestation.Web.Models;

namespace SelfAttestation.Web.Services.Interfaces
{
    public interface IAttestationService
    {
        Task<UserInfo> SubmitAttestationForm(UserInfo user);
        Task<List<Instruction>> GetAttestationText();
    }
}
