﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SelfAttestation.Web.Models;

namespace SelfAttestation.Web.Services.Interfaces
{
    public interface IFaqService
    {
        Task<List<Faqs>> GetFaqInfo();
    }
}
