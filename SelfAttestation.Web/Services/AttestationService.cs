﻿using Microsoft.Extensions.Configuration;
using SelfAttestation.Web.Models;
using SelfAttestation.Web.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelfAttestation.Web.Services
{
    public class AttestationService : BaseService, IAttestationService
    {
        private readonly IConfiguration _config;

        public AttestationService(IConfiguration config)
        {
            _config = config;
        }
        public async Task<UserInfo> SubmitAttestationForm(UserInfo user)
        {
            var serviceUri = $"{_config["AppSettings:RestUrl"]}/{_config["AppSettings:SubmitAttestationForm"]}";

            return await PostResponse<UserInfo, UserInfo>(serviceUri, user, false, new Dictionary<string, string> {
                { "employee", user.Username },
                { "ResultType", user.ResultType },
                { "AttestationDate", user.Date.ToString() },
                { "ResultFlag", user.ResultFlag.ToString() }
            });
        }

        public async Task<List<Instruction>> GetAttestationText()
        {
            var serviceUri = $"{_config["AppSettings:RestUrl"]}/{_config["AppSettings:GetAttestationText"]}";
            return await GetResponse<List<Instruction>>(serviceUri, new Dictionary<string, string> { });
        }
    }
}
