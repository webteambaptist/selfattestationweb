﻿var user;

$(document).ready(function () {
    $('#confirmBoxOptions').show();
    $('#needADUsername').hide();
    $('#password').hide();
    $('#sharedworkstation').hide();

    doConfirm("Are you currently using a WOW, Shared Workstation/LSID?",
        function yes() {
            $('#needADUsername').show();
            $('#password').show();
            $('#usingLSID').val("true");

            $('#confirmBoxOptions').hide();
        },
        function no() {
            $('#usingLSID').val("false");

            $.ajax({
                type: "GET",
                url: "UserNotUsingLSID",
                success: function (response) {
                    var userName = response;
                    $('#needADUsername').show();
                    $('#password').hide();
                    $('#sharedworkstation').show();
                    user = userName;
                    $('#attestUsername').val(userName);
                    $("#attestUsername").prop("readonly", true);

                    $('#confirmBoxOptions').hide();

                    document.getElementById('loginPassword').required = false;
                }
            });
        });
});

function doConfirm(msg, yesFn, noFn) {
    var confirmBox = $("#confirmBox");

    confirmBox.find(".message").text(msg);
    confirmBox.find(".yes,.no").unbind().click(function () {
        confirmBox.hide();
    });

    confirmBox.find(".yes").click(yesFn);
    confirmBox.find(".no").click(noFn);
    confirmBox.show();
}

$('#btnSubmit').click(function () {
    var result;
    var isFormValid;
    var selectedOption = $("input:radio:checked").val();
    //var user = $('#username').val();

    var isUsingLSID = $('#usingLSID').val();

    if (isUsingLSID === "" || isUsingLSID === null && selectedOption === undefined) {
        alert('You must select whether you are using an LSID Device or not in order to proceed.');
    }

    if (isUsingLSID === "false") {
        if (selectedOption === "Agree" && isUsingLSID !== "") {
            result = confirm('I AGREE, I do not have COVID-19 symptoms, and will receive an email confirmation.');
        }

        else if (selectedOption === "Disagree" && isUsingLSID !== "") {
            result = confirm('I am indicating that I may be exhibiting COVID-19 symptoms, and will receive email confirmation, along with my manager and Employee Health.');
        }
    }
    else if (isUsingLSID === "true") {
        if (selectedOption === "Agree" && isUsingLSID !== "") {
            result = confirm('I AGREE, I do not have COVID-19 symptoms, and will receive an email confirmation.');
        }

        else if (selectedOption === "Disagree" && isUsingLSID !== "") {
            result = confirm('I am indicating that I may be exhibiting COVID-19 symptoms, and will receive email confirmation, along with my manager and Employee Health.');
        }
    }

    if (result) {
        isFormValid = validateForm(isUsingLSID);
        if (!isFormValid) {
            alert('All fields must be filled in before submission');
        }
        else {
            $("form").submit();
        }
    }
});

function validateForm(isUsingLSID) {
    var isValid = true;
    if (isUsingLSID === "false") {
        $('#loginPassword').remove();
    }

    $('form input').each(function () {
        if ($(this).val() === '')
            isValid = false;
    });
    return isValid;
}
